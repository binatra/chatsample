package com.minci.mychattest;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Minci on 1/18/2017.
 */

public class MessageModel {

    @SerializedName("fromUserId")
    private String fromUserId;

    @SerializedName("toUserId")
    private String toUserId;

    @SerializedName("message")
    private String message;

    @SerializedName("senderName")
    private String senderName;

    public MessageModel(String fromUserId, String toUserId, String message) {
        this.fromUserId = fromUserId;
        this.toUserId = toUserId;
        this.message = message;
    }

    public String getFromUserId() {
        return fromUserId;
    }

    public void setFromUserId(String fromUserId) {
        this.fromUserId = fromUserId;
    }

    public String getToUserId() {
        return toUserId;
    }

    public void setToUserId(String toUserId) {
        this.toUserId = toUserId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "MessageModel{" +
                "fromUserId=" + fromUserId +
                ", toUserId=" + toUserId +
                ", message='" + message + '\'' +
                '}';
    }
}
