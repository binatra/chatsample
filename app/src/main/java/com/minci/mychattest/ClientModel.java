package com.minci.mychattest;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Minci on 1/18/2017.
 */
public class ClientModel {

    @SerializedName("userId")
    private String userId;

    @SerializedName("socketId")
    private String sockketId;

    @SerializedName("name")
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ClientModel(String userId, String sockketId, String name) {
        this.userId = userId;
        this.sockketId = sockketId;
        this.name = name;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getSockketId() {
        return sockketId;
    }

    public void setSockketId(String sockketId) {
        this.sockketId = sockketId;
    }

    @Override
    public String toString() {
        return "ClientModel{" +
                "userId=" + userId +
                ", sockketId=" + sockketId +
                ", name='" + name + '\'' +
                '}';
    }
}
