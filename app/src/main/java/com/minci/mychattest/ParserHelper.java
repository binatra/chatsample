package com.minci.mychattest;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by Minci on 1/18/2017.
 */

public class ParserHelper {
    //helper methods
    public static JSONObject jsonify(Object object) {
        try {
            return new JSONObject(new Gson().toJson(object));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static <T> T parseSingleArgument(Class<T> tClass, Object... args) {
        return new Gson().fromJson(args[0].toString(), tClass);
    }

    public static List<ClientModel> parseClientList(Object[] args) {
        String json = args[0].toString();
        return new Gson().fromJson(json, new TypeToken<List<ClientModel>>() {
        }.getType());
    }

}
