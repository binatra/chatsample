package com.minci.mychattest;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.net.URISyntaxException;
import java.util.List;
import java.util.UUID;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.socket.client.IO;
import io.socket.client.Socket;

public class MainActivity extends AppCompatActivity {

    private Socket socket;

    private final String USER_ID = UUID.randomUUID().toString();

    private final String NAME = "Artan";

    @BindView(R.id.send_btn)
    Button sendButton;

    @BindView(R.id.text_to_view)
    TextView textSended;

    @BindView(R.id.text_to_send)
    EditText textToSend;

    @BindView(R.id.text_recieved)
    TextView textRecieved;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        connectToSocketIO();
        socket.connect();
        subscribeOnSocketEvents();
        subscribeOnSocketLifecycleMethods();

    }

    private void connectToSocketIO() {
        try {
            socket = IO.socket("http://192.168.1.126:3000");
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    private void subscribeOnSocketEvents(){
        socket.on("clientList", args -> {
            List<ClientModel> clientModels = ParserHelper.parseClientList(args);
            for(ClientModel clientModel : clientModels){
                Log.d("ClientModel", clientModel.toString());
            }
        });

        socket.on("recieveMessage", args -> {
           MessageModel messageModel = ParserHelper.parseSingleArgument(MessageModel.class, args);
            Log.d("Message", messageModel.toString());
            textRecieved.setText(messageModel.getMessage());
        });
    }


    private void subscribeOnSocketLifecycleMethods() {
        socket.on(Socket.EVENT_CONNECT, args -> {
            Log.d("SocketIo", "event_connect, socketId");



            UserIdModel model = new UserIdModel(USER_ID, NAME);
            socket.emit("storeClientInfo", ParserHelper.jsonify(model));
            runOnUiThread(() -> {
                sendButton.setBackgroundColor(getResources().getColor(R.color.colorConnected));
            });



        });

        socket.on(Socket.EVENT_DISCONNECT, args -> {
            Log.d("SocketIo", "event_disconnect");

            runOnUiThread(() -> {
                sendButton.setBackgroundColor(getResources().getColor(R.color.colorDisconnected));
            });

        });
        socket.on(Socket.EVENT_ERROR, args -> {
            Log.d("SocketIo", "event_error" + args[0].toString());
        });
        socket.on(Socket.EVENT_CONNECTING, args -> {
            Log.d("SocketIo", "event_connecting");

        });
        socket.on(Socket.EVENT_CONNECT_ERROR, args -> {
            Log.d("SocketIo", "event_connect_error " + args[0].toString());
        });
        socket.on(Socket.EVENT_CONNECT_TIMEOUT, args -> {
            Log.d("SocketIo", "event_connect_timeout");
        });
        socket.on(Socket.EVENT_RECONNECT, args -> {
            Log.d("SocketIo", "event_reconnect");
            runOnUiThread(() -> {
                sendButton.setBackgroundColor(getResources().getColor(R.color.colorReconnecting));
            });
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        socket.disconnect();
        socket.off();
    }

    @OnClick(R.id.send_btn)
    public void onClickSend(View view){
        Toast.makeText(this, "hjellooo", Toast.LENGTH_SHORT).show();
        socket.emit("sendMessage", ParserHelper.jsonify(new MessageModel(USER_ID,USER_ID,textToSend.getText().toString())));
        textSended.setText(textToSend.getText());
        textToSend.setText("");
    }
}
