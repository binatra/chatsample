package com.minci.mychattest;

/**
 * Created by Minci on 1/18/2017.
 */

public class UserIdModel {

    private final String userId;

    private final String name;

    public UserIdModel(String userId, String name) {
        this.userId = userId;
        this.name = name;
    }
}
